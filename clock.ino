#include "heltec.h"
#include <WiFi.h>
#include "time.h"

#include <WiFiUdp.h>
#include <NTPClient.h>

const char *ssid = "Wild Old West";
const char *password = "playstation8";

WiFiUDP ntpUDP;

// You can specify the time server pool and the offset (in seconds, can be
// changed later with setTimeOffset() ). Additionally you can specify the
// update interval (in milliseconds, can be changed using setUpdateInterval() ).
NTPClient timeClient(ntpUDP, "north-america.pool.ntp.org", -18000); // updates once an hour and is offset for EST (-5)

void setup()
{
	Serial.begin(115200);

	Heltec.begin(true /*DisplayEnable Enable*/, false /*LoRa Disable*/, true /*Serial Enable*/);
	Heltec.display->init();

	// Connect to Wi-Fi
	Serial.printf("Connecting to %s...", ssid);
	WiFi.begin(ssid, password);
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(1000);
		Serial.print(".");
	}
	Serial.println(" CONNECTED");
	timeClient.begin();
}

void drawTime()
{
	Heltec.display->clear();
	Heltec.display->setTextAlignment(TEXT_ALIGN_CENTER);
	Heltec.display->setFont(ArialMT_Plain_24);
	Heltec.display->drawString(128 / 2, 32 - 16, timeClient.getFormattedTime());
	Heltec.display->display();
	Serial.println(timeClient.getFormattedTime());
}

void loop()
{
	while (timeClient.isTimeSet() != 1)
	{
		delay(1000);
		timeClient.update();
		timeClient.forceUpdate();
		Serial.println("timeClient forceUpdate() called!");
	}
	delay(500);
	drawTime();
}
